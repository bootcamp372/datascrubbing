﻿using System.Diagnostics;
using System.Numerics;

namespace DataScrubbing
{
    internal class Program
    {

        static void Main(string[] args)
        {

            string command = "Y";
            char charToRemove;
            char charToReplaceWith;


            while (command == "Y")
            {
                if (command == "Y")
                {
                    try
                    {
                        Console.Write("Enter Phone Number: ");
                        string phone = Console.ReadLine();
                        Console.WriteLine("Choose Scrub Method: ");
                        Console.WriteLine();
                        Console.WriteLine("(1) Remove Leading and Trailing Spaces - Enter 1");
                        Console.WriteLine();
                        Console.WriteLine("(2) Remove Leading and Trailing Spaces and a character - Enter 2");
                        Console.WriteLine();
                        Console.WriteLine("(3) Remove Leading and Trailing Spaces and a character, replace with another character - Enter 3");
                        string scrubMethod = Console.ReadLine();
                        switch (scrubMethod)

                        {
                            case "1":
                                ScrubPhone(ref phone);
                                break;
                            case "2":
                                Console.WriteLine("Enter characters to remove:");
                                charToRemove =  char.Parse(Console.ReadLine());
                                ScrubPhone(ref phone, ref charToRemove);
                                break;
                            case "3":
                                Console.WriteLine("Enter character to remove:");
                                charToRemove = char.Parse(Console.ReadLine());
                                Console.WriteLine("Enter character to replace with:");
                                charToReplaceWith = char.Parse(Console.ReadLine());
                                ScrubPhone(ref phone, ref charToRemove, ref charToReplaceWith);
                                break;
                            default:
                                Console.WriteLine("Invalid Input");
                                break;
                        }
                        
                        Console.Write("Do you want to enter another phone number? Enter Y or N:");
                        command = Console.ReadLine().ToUpper();

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: " + e);
                        return;
                    }                    
                }
                else if (command == "N")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("**Error: unrecognized command");
                }
            }
        }

        public static void ScrubPhone(ref string phone)
        {
            string scrubbedPhone = phone.Trim();
            Console.WriteLine("Scrubbed Phone: {0}", scrubbedPhone);
/*            Console.WriteLine("Scrubbed Phone: " + scrubbedPhone);*/
        }

        public static void ScrubPhone(ref string phoneNumber, ref char charToRemove)
        {
            int pos = phoneNumber.IndexOf(charToRemove);
            string scrubbedPhone = phoneNumber.Replace(charToRemove.ToString(), string.Empty).Trim();
            Console.WriteLine("Scrubbed Phone: {0}", scrubbedPhone);
        }
        public static void ScrubPhone(ref string phoneNumber, ref char charToRemove, ref char charToReplaceWith)
        {
            string scrubbedPhone = phoneNumber.Replace(charToRemove, charToReplaceWith).Trim();
            Console.WriteLine("Scrubbed Phone: {0}", scrubbedPhone);
        }
    }
}